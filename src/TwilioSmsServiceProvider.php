<?php

namespace ThisWay\TwilioSms;

use Illuminate\Support\ServiceProvider;
use Services_Twilio;

class TwilioSmsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('sms', function($app){
            if (
                !config('twilio-sms.account_sid') ||
                !config('twilio-sms.auth_token') ||
                !config('twilio-sms.phone_number')
            )
            {
                throw new Exception('Twilio-SMS library might not be configured. Did you publish the vendor packages?');
            }

            return new TwilioSmsService(
                new Services_Twilio(
                    config('twilio-sms.account_sid'),
                    config('twilio-sms.auth_token')
                ),
                config('twilio-sms.phone_number')
            );
        });
    }

    public function boot()
    {
        // Publish the configuration
        $this->publishes([
            __DIR__ . '/../config/twilio-sms.php'   => config_path('twilio-sms.php'),
        ], 'config');

        // Add a route for testing the package
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/routes.php';
        }
    }
}