<?php

namespace ThisWay\TwilioSms;

class TwilioSmsService
{
    protected $smsProvider;
    protected $phoneNumber;

    public function __construct($smsProvider, $phoneNumber)
    {
        $this->setSmsProvider($smsProvider)
            ->setPhoneNumber($phoneNumber);
    }

    /**
     * @param mixed $smsProvider
     * @return $this
     */
    public function setSmsProvider($smsProvider)
    {
        $this->smsProvider = $smsProvider;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSmsProvider()
    {
        return $this->smsProvider;
    }

    /**
     * @param mixed $phoneNumber
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }


    public function sendValidationCode($phoneNumber)
    {
        return $this->sendSms($phoneNumber, static::getRandomValidationCode());
    }

    public function sendSms($phoneNumber, $message)
    {
        return $this->getSmsProvider()
                ->account
                ->messages
                ->create(array(
                    "From"  => $this->getPhoneNumber(),
                    "To"    => $phoneNumber,
                    "Body"  => $message,
                ));
    }

    public static function getRandomValidationCode()
    {
        return substr(uniqid(TRUE), rand(0,16),6);
    }
}