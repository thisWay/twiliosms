<?php

Route::get('/test-twilio-sms/{phoneNumber}', function ($phoneNumber) {
    app('sms')->sendValidationCode($phoneNumber);
});